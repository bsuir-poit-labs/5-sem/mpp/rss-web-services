﻿using System.Collections.Generic;
using System.IO;
using System.Net;
using System.Text;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;
using RSS_Console_feed_reader.Config;
using RSS_Console_feed_reader.Entity;

namespace RSS_filter.Controllers
{
    [ApiController]
    [Route("[controller]")]
    public class FilterController : ControllerBase
    {
        [HttpGet]
        public IActionResult Get(string url, string keywords)
        {
            var request = (HttpWebRequest) WebRequest.Create($"{WebServicesConfig.FeedLoaderAddress}?url={url}");
            // request.Accept = "application/xml";

            var response = request.GetResponse();

            using var streamReader = new StreamReader(response.GetResponseStream()!, Encoding.UTF8);
            var feedItemList = JsonConvert.DeserializeObject<FeedItemList>(streamReader.ReadToEnd());

            var keywordsList = keywords.Split(',');
            var filterFeedItemList = FilterFeed(feedItemList, keywordsList);

            return Ok(filterFeedItemList);
        }

        private static FeedItemList FilterFeed(FeedItemList feedItemList, string[] keyWords)
        {
            if (keyWords.Length == 0)
            {
                return feedItemList;
            }

            var feedItems = feedItemList.Items.ToArray();
            var filterFeedItems = new List<FeedItem>();

            foreach (var keyWord in keyWords)
            {
                foreach (var item in feedItems)
                {
                    if (item.Summary.ToUpper().Contains(keyWord.ToUpper()) && !filterFeedItems.Contains(item))
                    {
                        filterFeedItems.Add(item);
                    }
                }
            }

            feedItemList.Items = filterFeedItems;
            return feedItemList;
        }
    }
}