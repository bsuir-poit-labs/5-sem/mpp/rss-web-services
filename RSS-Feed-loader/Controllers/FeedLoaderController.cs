﻿using System.Linq;
using System.ServiceModel.Syndication;
using System.Text.RegularExpressions;
using System.Xml;
using Microsoft.AspNetCore.Mvc;
using RSS_Console_feed_reader.Entity;

namespace RSS_Feed_loader.Controllers
{
    [ApiController]
    [Route("[controller]")]
    public class FeedLoaderController : ControllerBase
    {
        private static readonly Regex Regex = new Regex(@"(<.*\/>)(.*)(<.*\/>)");

        [HttpGet]
        public IActionResult Get(string url)
        {
            var feed = SyndicationFeed.Load(XmlReader.Create(url));

            var feedItemList = new FeedItemList();

            foreach (var item in feed.Items)
            {
                var match = Regex.Match(item.Summary.Text);
                var summary = match.Groups[2].Value;

                feedItemList.Items.Add(new FeedItem
                {
                    Title = item.Title.Text,
                    Summary = summary,
                    PubDate = item.PublishDate.ToString("d MMM yyyy") + "г.",
                    Link = item.Links.First().Uri.ToString()
                });
            }

            return Ok(feedItemList);
        }
    }
}