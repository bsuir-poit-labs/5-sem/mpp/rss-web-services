﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using Newtonsoft.Json;
using RSS_Console_feed_reader.Config;
using RSS_Console_feed_reader.Entity;
using RSS_Mail_sender.Mail;

namespace RSS_Mail_sender.Controllers
{
    [ApiController]
    [Route("[controller]")]
    public class MailSenderController : ControllerBase
    {
        [HttpGet]
        public IActionResult Get(string url, string keywords, string emails)
        {
            var emailList = emails.Split(' ');
            var feed = GetFeedItemList(url, keywords);

            foreach (var email in emailList)
            {
                MailService.SendEmail(email, feed.ToString());
            }

            return Ok();
        }
        
        private static FeedItemList GetFeedItemList(string url, string keyWords)
        {
            var request = (HttpWebRequest) WebRequest.Create(
                $"{WebServicesConfig.FilterServiceAddress}?url={url}&keywords={keyWords}");

            var response = request.GetResponse();

            using var streamReader =
                new StreamReader(response.GetResponseStream() ?? throw new InvalidOperationException(), Encoding.UTF8);
            var feedItemList = JsonConvert.DeserializeObject<FeedItemList>(streamReader.ReadToEnd());
            return feedItemList;
        }
    }
}