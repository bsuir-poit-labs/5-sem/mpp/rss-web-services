﻿using System.Net;
using System.Net.Mail;

namespace RSS_Mail_sender.Mail
{
    public class MailService
    {
        public static void SendEmail(string email, string message)
        {
            var fromEmail = new MailAddress(MailConfig.EmailFrom);
            var toEmail = new MailAddress(email);

            var mailMessage = new MailMessage(fromEmail, toEmail)
            {
                Subject = $"RSS mailing", Body = message
            };

            var smtpClient = new SmtpClient(MailConfig.SmtpHost, MailConfig.SmtpPort)
            {
                UseDefaultCredentials = false,
                Credentials = new NetworkCredential(MailConfig.EmailFrom, MailConfig.PasswordFrom),
                EnableSsl = true
            };

            smtpClient.Send(mailMessage);
        }
    }
}