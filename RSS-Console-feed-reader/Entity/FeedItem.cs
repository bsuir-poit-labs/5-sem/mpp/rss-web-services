﻿namespace RSS_Console_feed_reader.Entity
{
    public class FeedItem
    {
        public string Title { get; set; }

        public string Summary { get; set; }

        public string PubDate { get; set; }
        
        public string Link { get; set; }
    }
}