﻿using System.Collections.Generic;
using System.Text;
using System.Xml.Serialization;

namespace RSS_Console_feed_reader.Entity
{
    public class FeedItemList
    {
        public List<FeedItem> Items { get; set; }

        public FeedItemList()
        {
            Items = new List<FeedItem>();
        }

        public override string ToString()
        {
            var stringBuilder = new StringBuilder();

            foreach (var item in Items)
            {
                stringBuilder.Append($"\t{item.Title.ToUpper()}\n")
                    .Append($"{item.Summary}\n")
                    .Append($"{item.PubDate}г. ")
                    .Append($"{item.Link}\n\n");
            }

            return stringBuilder.ToString();
        }
    }
}