﻿namespace RSS_Console_feed_reader.Config
{
    public static class WebServicesConfig
    {
        public const string EmailServiceAddress = "https://localhost:44351/mailsender";
        
        public const string FilterServiceAddress = "https://localhost:44378/filter";
        
        public const string FeedLoaderAddress = "https://localhost:44336/feedloader";
    }
}