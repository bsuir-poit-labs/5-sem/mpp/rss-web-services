﻿using System;
using System.IO;
using System.Net;
using System.Text;
using System.Threading;
using Newtonsoft.Json;
using RSS_Console_feed_reader.Config;
using RSS_Console_feed_reader.Entity;

namespace RSS_Console_feed_reader.Rss
{
    public static class RssFeed
    {
        public static void PerformFeed(string url, string keyWords, string emails)
        {
            var waitHandler = new ManualResetEvent(false);
            FeedItemList feedItemList = null;

            ThreadPool.QueueUserWorkItem(_ =>
            {
                feedItemList = GetFeedItemList(url, keyWords);
                waitHandler.Set();
            });
            waitHandler.WaitOne();

            var strFeed = feedItemList.ToString();

            ThreadPool.QueueUserWorkItem(_ => SendFeed(url, keyWords, emails));

            ThreadPool.QueueUserWorkItem(_ => Console.WriteLine(strFeed));
        }

        private static FeedItemList GetFeedItemList(string url, string keyWords)
        {
            var request = (HttpWebRequest) WebRequest.Create(
                $"{WebServicesConfig.FilterServiceAddress}?url={url}&keywords={keyWords}");

            var response = request.GetResponse();

            using (var streamReader =
                new StreamReader(response.GetResponseStream() ?? throw new InvalidOperationException(), Encoding.UTF8))
            {
                var feedItemList = JsonConvert.DeserializeObject<FeedItemList>(streamReader.ReadToEnd());
                return feedItemList;
            }
        }

        private static bool SendFeed(string url, string keywords, string emails)
        {
            var request = (HttpWebRequest) WebRequest.Create(
                $"{WebServicesConfig.EmailServiceAddress}?url={url}&keywords={keywords}&emails={emails}");
            // request.Accept = "application/xml";

            var response = (HttpWebResponse) request.GetResponse();

            return response.StatusCode == HttpStatusCode.OK;
        }
    }
}