﻿using System;
using System.Threading;
using RSS_Console_feed_reader.Rss;

namespace RSS_Console_feed_reader
{
    internal class Program
    {
        private const int ExitValue = 2;
        
        private static Timer _timer;

        private static string _url;
        private static string _keyWords;
        private static string _emails;
        private static long _period;

        static void Main()
        {
            var action = 0;
            do
            {
                ShowMenu();
                try
                {
                    action = Convert.ToInt32(Console.ReadLine());
                }
                catch (Exception)
                {
                    Console.WriteLine("Некорректный ввод!");
                }

                switch (action)
                {
                    case 1:
                        PrepareFeed();
                        break;
                    case 2:
                        break;
                    default:
                        Console.WriteLine("Некорректный ввод!");
                        break;
                }
            } while (action != ExitValue);
        }

        private static void PrepareFeed()
        {
            Console.Write("Укажите url RSS источника: ");
            _url = Console.ReadLine();

            Console.Write("Укажите ключевые слова через запятую: ");
            _keyWords = Console.ReadLine();

            Console.Write("Укажите почтовые адреса для рассылки через пробел: ");
            _emails = Console.ReadLine();

            Console.Write("Укажите период опроса RSS источников в (мс): ");
            _period = Convert.ToInt64(Console.ReadLine());

            Console.WriteLine();

            _timer = new Timer(CallBackTimer, null, 0, _period);

            Console.ReadKey();

            _timer.Dispose();
        }

        private static void CallBackTimer(object state)
        {
            var thread = new Thread(_ => RssFeed.PerformFeed(_url, _keyWords, _emails));
            thread.Start();
        }

        private static void ShowMenu()
        {
            Console.Clear();
            Console.WriteLine("1. Начать рассылку RSS новостей.");
            Console.WriteLine("2. Выход.");
            Console.Write("Выберите действие: ");
        }
    }
}